import L from "leaflet";
import "leaflet-routing-machine";
import "leaflet-routing-machine/dist/leaflet-routing-machine.css";
import { useEffect, useState } from "react";
import { useMap } from "react-leaflet";
import { useLocation } from "react-router-dom";

L.Marker.prototype.options.icon = L.icon({
    iconUrl: "https://unpkg.com/leaflet@1.7.1/dist/images/marker-icon.png"
});

export default function Routing() {
    const [brukerPosisjon, setBrukerPosisjon] = useState([]);
    const [hasFetchedLoc, setHasFetchedLoc] = useState(false);
    const location = useLocation();
    const { places } = location.state;
    const pos = [63.4149814, 10.4062945];
    // useEffect(() => {
    //     const fetchLocation = async () => {
    //         try {
    //             await navigator.geolocation.getCurrentPosition(
    //                 (position) => {
    //                     setBrukerPosisjon([
    //                         position.coords.latitude,
    //                         position.coords.longitude
    //                     ]);
    //                     setHasFetchedLoc(true);
    //                 }
    //             );
    //         } catch (error) {
    //             console.log("Error fetching location:", error);
    //         }
    //     };

    //     if (!hasFetchedLoc) {
    //         fetchLocation();
    //     }
    // }, [hasFetchedLoc]);

    const map = useMap();

    useEffect(() => {
        if (!map) return;
        const routingControl = L.Routing.control({
            waypoints: [
                L.latLng(pos[0], pos[1]),
                L.latLng(places[0][4], places[0][3])
            ],
            routeWhileDragging: true,
            show: false,
            collapsible: true,
            createMarker: function (i, wp, n) {
                return false;
            }
        }).addTo(map);

        return () => map.removeControl(routingControl);
    }, [map, places]);

    return null;
}
