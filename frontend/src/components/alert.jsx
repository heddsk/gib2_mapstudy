import React, { useState } from "react";




function Alert(){
    const [isClose, setClosed] = useState("false"); 
    const handleClose= () => {
        setClosed(!isClose);
        };
    return(
        <div>
            <div id={isClose ? "alertGray" : "alertGone"}>
                <p>j</p>
            </div>
            <div className={isClose ? "fragment": "fragmentX"}>
            
                <button id="close" onClick={handleClose}>x</button>
                <h3>Velkommen til Studieplassen!</h3>
                <p >
                    Her ser du en oversikt over lesesalene i Trondheim. Markørene for lesesalene er samlet sammen. 
                    Zoom inn eller trykk på fargeklattene for å finne lessalene. 
                </p>
                <p>
                For å finne den beste studieplassene for deg, trykk på "Finn studieplass" øverst til venstre. 
                </p> 

                <p>
                For å legge til eller endre vurdering på lesesaler trykker du på markøren til lesesalen. 
                </p>

            </div>
        </div>
    )
    };
export default Alert;
