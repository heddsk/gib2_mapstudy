import "../css/login.css";
import NavBar from "./navBar";
import pointerIcon from "../static/icons/Point2.png"
import { useNavigate } from "react-router-dom";
export default function OmSiden(){

  const navigate = useNavigate();
  return(
    
    <div>
      <NavBar></NavBar>
      <header className="header" id="headerOm">
        <a onClick={() => navigate('/home')} className="button" id="tilbake"> Tilbake til hovedsiden</a>
        <h1>Informasjon om Studieplassen!</h1>
      </header>
      <div className ="box" id="infoSide">
        <p >
          På vår nettside finner du en oversikt over lesesalene i Trondheim på NTNU. 
        </p>
        <p>
          For å finne den beste studieplassene for deg, trykk på "Finn studieplass" øverst til venstre. 
          Her kan du velge hvilke egenskaper du ønsker at rommet skal ha og om det er viktig for deg 
          at andre har gitt plassen en høy vurdering. 
          I tillegg
          kan du huke av de egenskapene som er ekstra viktige for deg!
        </p> 

        <p id="styleIcon">Finn studieplass</p> 
        <p>
          Om du ønsker å endre eller oppdatere informasjonen til en sal trykker du på markøren til lessalen på hovedsiden. Da endrer du verdiene til den
          informasjonen du ønsker å lagre og trykker "Legg Til" så er lesesalen oppdatert!
        </p>
      <img src={pointerIcon} alt="Logo" style={{width:"80px", height:"80px"}} />
      </div>
    </div>
  )
};

// export default function Login() {
//   const [email, setEmail] = useState("");
//   const [password, setPassword] = useState("");
//   function validateForm() {
//     return email.length > 0 && password.length > 0;
//   }
//   function handleSubmit(event) {
//     event.preventDefault();
//   }
//   return (
//     <div>
//     <NavBar></NavBar>
//     <div>
//       <Form className="Login" onSubmit={handleSubmit}>
//         <Form.Group className="email" size="lg" controlId="email">
//           <Form.Label className="email">Email</Form.Label>
//           <br></br>
//           <Form.Control
//             autoFocus
//             className="Text"
//             type="text"
//             value={email}
//             onChange={(e) => setEmail(e.target.value)}
//             />
//         </Form.Group>
//         <Form.Group size="lg" controlId="password">
//           <Form.Label className="email">Password</Form.Label>
//           <br></br>
//           <Form.Control
//             className="Text"
//             type="password"
//             value={password}
//             onChange={(e) => setPassword(e.target.value)}
//             />
//         </Form.Group>
//         <Button block size="lg" type="submit" disabled={!validateForm()}>
//           Login
//         </Button>
//       </Form>
//             </div>
//     </div>
//   );
// }