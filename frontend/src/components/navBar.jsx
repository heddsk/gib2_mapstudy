import "leaflet/dist/leaflet.css";
import { useNavigate } from 'react-router-dom';

import React from 'react';

function NavBar(){
    const navigate = useNavigate();
    function handleCliskFilter (){
        navigate("/filterSite");
    }
    function handleCliskHome (){
        navigate("/home");
    }
    function handleCliskOm (){
        navigate("/omSiden");
    }
    return(
        <div className ="navBar">
            <a id="filterSite" onClick={handleCliskFilter}>Finn studieplass</a>
             <div className="topnav-centered">
                 <a id="home" onClick={handleCliskHome}>Studieplassen</a>
            </div>
            <a id="OmSiden"onClick={handleCliskOm}>Om siden</a>
        </div>
    )
};
export default NavBar;