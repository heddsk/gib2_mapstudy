import L from "leaflet";
import "leaflet/dist/leaflet.css";
import React, { useEffect, useRef, useState } from "react";
import { MapContainer, Marker, TileLayer, useMapEvent } from "react-leaflet";
import MarkersResult from "./markersResult";

import Routing from "./routing";

const iconYou = L.icon({
    iconUrl: require("../static/icons/Pointer.png"),
    iconSize: 50
});

function SetViewOnClick({ animateRef }) {
    const map = useMapEvent("click", (e) => {
        map.setView(e.latlng, map.getZoom(), {
            animate: animateRef.current || false
        });
    });

    return null;
}

function MapResult() {
    const animateRef = useRef(false);
    const pos = [63.4149814, 10.4062945];
    // const [brukerPosisjon, setBrukerPosisjon] = useState([]);
    // const [hasFetchedLoc, setHasFetchedLoc] = useState(false);
    // useEffect(() => {
    //     const fetchLocation = async () => {
    //         try {
    //             await navigator.geolocation.getCurrentPosition(
    //                 (position) => {
    //                     setBrukerPosisjon([
    //                         position.coords.latitude,
    //                         position.coords.longitude
    //                     ]);
    //                     setHasFetchedLoc(true);
    //                 }
    //             );
    //         } catch (error) {
    //             console.log("Error fetching location:", error);
    //         }
    //     };

    //     if (!hasFetchedLoc) {
    //         fetchLocation();
    //     }
    // }, [hasFetchedLoc]);

    // const createRoutingMachineLayer = () => {
    //     const instance = L.Routing.control({
    //         position: "topleft",
    //         waypoints: [
    //             L.latLng(38.9072, -77.036),
    //             L.latLng(37.7749, -122.4194)
    //         ],
    //         lineOptions: {
    //             styles: [
    //                 {
    //                     color: "#757de8"
    //                 }
    //             ]
    //         }
    //     });
    //     return instance;
    // };
    // const RoutingMachine = createControlComponent(createRoutingMachineLayer);

    // const coordYou = [brukerPosisjon.latitude, brukerPosisjon.longitude]
    console.log("coord", pos);
    // if (!hasFetchedLoc) {
    //     return <div>Loading...</div>;
    // }
    return (
        <MapContainer
            id="mapR"
            center={pos}
            zoom={14}
            scrollWheelZoom={true}
        >
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Routing />
            <Marker position={pos} icon={iconYou}></Marker>
            <MarkersResult></MarkersResult>
            <SetViewOnClick animateRef={animateRef} />
        </MapContainer>
    );
}
export default MapResult;
