
import "leaflet/dist/leaflet.css";
import React, { useRef } from "react";
import {
    MapContainer,
    TileLayer,
    useMapEvent,
} from "react-leaflet";
import MarkerClusterGroup from "react-leaflet-cluster";
import Markers from "./markers";


function SetViewOnClick({ animateRef }) {
    const map = useMapEvent("click", (e) => {
        map.setView(e.latlng, map.getZoom(), {
            animate: animateRef.current || false
        });
    });

    return null;
}

function Map() {
    const animateRef = useRef(false);

    return (
        <MapContainer
            id="map"
            center={[63.416877, 10.405647]}
            zoom={14}
            scrollWheelZoom={true}
        >
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <MarkerClusterGroup>
                <Markers></Markers>
            </MarkerClusterGroup>
            
            <SetViewOnClick animateRef={animateRef} />
        </MapContainer>
    );
}
export default Map;
