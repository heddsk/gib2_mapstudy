import "leaflet/dist/leaflet.css";
import React from "react";
import "../App.css"
import Map from "./map";
import NavBar from "./navBar";
import "../css/map.css";
import "../css/page.css";
import "../css/navBar.css";
import "../css/addPlace.css";



function HomeApp() {
    return (
        <div id="app">
            <NavBar></NavBar>
            <Map></Map>
        </div>
    );
}
export default HomeApp;
